package ictgradschool.web.lab10.exercise01;

import ictgradschool.web.lab10.exercise04.UserPojo;
import ictgradschool.web.lab10.utilities.HtmlHelper;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;

public class UserDetailsServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");

        HttpSession session = request.getSession(true);

        UserPojo upojo = new UserPojo();

        String fnameID = request.getParameter("firstname");
        String lnameID = request.getParameter("lastname");
        String countryID = request.getParameter("country");
        String cityID = request.getParameter("city");

        String paragraphID = "Welcome"+" "+fnameID+" "+lnameID+" "+"from"+countryID+","+" "+cityID;

        // todo: add the remaining items to the upjo
        upojo.setFname(fnameID);
        upojo.setLname(lnameID);
        upojo.setCountry(countryID);
        upojo.setCity(cityID);

        session.setAttribute("user", upojo);


        Cookie a = new Cookie("firstname", fnameID);
        Cookie b = new Cookie("lastname", lnameID);
        Cookie c = new Cookie("country", countryID);
        Cookie d = new Cookie("city", cityID);

        response.addCookie(a);
        response.addCookie(b);
        response.addCookie(c);
        response.addCookie(d);


        PrintWriter out = response.getWriter();

        // Header stuff
        out.println(HtmlHelper.getHtmlPagePreamble("Web Lab 10 - Sessions"));

        out.println("<a href=\"index.html\">HOME</a><br>");

        out.println("<h3>Data entered: </h3>");
        //TODO - add the firstName, lastName, city and country  that were entered into the form to the list below
        //TODO - add the parameters from the form to session attributes
        out.println("<ul>");
        out.println("<li>First Name: " + fnameID + " </li>");
        out.println("<li>Last Name: " + lnameID + "  </li>");
        out.println("<li>Country: " + countryID + " </li>");
        out.println("<li>City: " + cityID + " </li>");
        out.println("</ul>");

        out.println(HtmlHelper.getHtmlPageFooter());
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Process POST requests the same as GET requests
        doGet(request, response);
    }
}
